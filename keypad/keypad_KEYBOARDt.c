#include <stdio.h>
#include <string.h>


int main()
{
    char keySeq[4];
    int counter=0, i=0,tries=2; 
    
    printf("Input 4-Digit Sequence: ");
    
    for(;tries >= 0;tries--){
        while(counter<=3){
            scanf(" %c", &keySeq[i]); //%c bc want only 1 char write to string up to 4
            //printf(" %s\n", keySeq);
            counter++;
            i++;
        }
        
        //Compare to desired code (3 tries)
        if(strcmp(keySeq, "1234") == 0){ //==0 so no differences;
        
        //TIMER START 
        /*
        int i;
        do{
            printf("Door will remain open for 30 seconds\n ");
            sleep(5);
        }
        
        for (i = 30; i > 0; i--){
        sleep(60);
        
        printf("- [%d] Seconds remaining until the door locks\n", i );
        }
        
        {
            printf("Zero\nDoor lock!\n");
        }
        
        return 0;
        */ //TIMER END
        
            printf("Pass, lock open. Welcome!\n");
            tries=3;
        }
        
        else if(tries != 0){
            printf("Fail, %d tries left.\n  Retry: ", tries);
        }
        
        else{
            printf("No more retries. Complete Shutdown.\n");
        }
        
        //Resets
        counter=0,i=0;
    }


    return 0;
}