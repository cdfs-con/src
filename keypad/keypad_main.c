#include <stdio.h>
#include <string.h>
#include <wiringPi.h>

#define ROWS 4
#define COLS 4

char keySeq[4];
char pressedKey = '\0';

int rowPins[ROWS] = {26,19,13,6}; //used GPIO
int colsPins[COLS] = {5,21,20,16};

int LED=24; //output gpio


char keys[ROWS][COLS] = {
   {'1', '2', '3', 'A'},
   {'4', '5', '6', 'B'},
   {'7', '8', '9', 'C'},
   {'*', '0', '#', 'D'}
};

void init_keypad()
{
   for (int c = 0; c < COLS; c++)
   {
      pinMode(colsPins[c], OUTPUT);   
      digitalWrite(colsPins[c], HIGH);
   }

   for (int r = 0; r < ROWS; r++)
   {
      pinMode(rowPins[r], INPUT);   
      pullUpDnControl(rowPins[r], PUD_UP);
   }
}

int findLowRow()
{
   for (int r = 0; r < ROWS; r++)
   {
      if (digitalRead(rowPins[r]) == LOW)
         return r;
   }

   return -1;
}

char get_key()
{
   int rowIndex;

   for (int c = 0; c < COLS; c++)
   {
      digitalWrite(colsPins[c], LOW);

      rowIndex = findLowRow();
      if (rowIndex > -1)
      {
         if (!pressedKey)
            pressedKey = keys[rowIndex][c];
         return pressedKey;
      }

      digitalWrite(colsPins[c], HIGH);
   }

   pressedKey = '\0';

   return pressedKey;
}

int main()
{
    wiringPiSetupGpio();
    pinMode(LED, OUTPUT);//MAY NOT NEED
    
    init_keypad();
    int counter=1, i=1,tries=2; //scan input counter;sequence controller; tries countdown
    
    printf("Input 4-Digit Sequence: ");

while(1){

//Scan code/inputs
    for(;tries >= 0;tries--){

        //GPIO read
        for(;counter<=4;counter++){
            keySeq[i] = get_key();
            
            if(keySeq){
                i++;
            }
            else{
                counter--;//prevent progression
            }
            //delay(200);
            
            printf(" %s\n", keySeq);
        }
        
//Compare to desired code (3 tries default)
        if(strcmp(keySeq, "1234") == 0){ //==0 so no differences
            printf("Pass\n");
            
            digitalWrite(LED, HIGH);
            tries=3;
        }
        else if(tries != 0){
            printf("Fail, %d tries left\n", tries);
            digitalWrite(LED, LOW);
        }
        else{
            printf("Fail, end o da line\n");
            digitalWrite(LED, LOW);
        }

//Resets
        //memset(keySeq,'0',4); //4 key input
        counter=1,i=1; //when fail, reset so loop can rescan

    return 0;
    }
}//while end

}