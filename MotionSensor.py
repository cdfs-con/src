# Raspberry pi code for MotionSensor
# Write your code here :-)
import RPi.GPIO as GPIO
from gpiozero import MotionSensor
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(27, GPIO.OUT)
GPIO.setup(17, GPIO.OUT)


pir = MotionSensor(4)


while True:
    pir.wait_for_motion()
    print("Motion detected!")
    
    #this buzzer pin # 16
    GPIO.setup(16, False)
    time.sleep(0.5)
    GPIO.setup(16, True)
    
    # 2 LED lights
    GPIO.output(27, False)
    GPIO.output(17, False)

    time.sleep(1/10)

    print("Light On")
    GPIO.output(27, True)
    GPIO.output(17, True)

    time.sleep(1/10)

    GPIO.output(27, False)
    GPIO.output(17, False)