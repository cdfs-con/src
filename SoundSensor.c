int main(void)
{
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* Infinite loop: Check infinitely while system is active */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    //Check if input is read from the mic, which is connected to pin 3 on NUCLEO-F303K8
	if(HAL_GPIO_ReadPin(GPIOA, Pass1_Pin))
	{
        //If true, Output comes to Pin 4 on NUCLEO-F303K8 in the form of an LED
        //For as long as sound is detected, the LED remains on
		HAL_GPIO_WritePin(GPIOA, State1_Pin, GPIO_PIN_SET);
	}
	else
	{
        //Else, Reset the LED, PinD12
		HAL_GPIO_WritePin(GPIOA, State1_Pin, GPIO_PIN_RESET);
	}
    //Add a 50 ms Delay
	HAL_Delay(50);
    /* USER CODE END WHILE */
   
  }
}
